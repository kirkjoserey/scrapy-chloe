import urllib
import urllib2  
import os  
import scrapy
import traceback
import zipfile

from os.path import basename  
from urlparse import urlsplit  
from scrapy.spider import BaseSpider  

class ChloeSpider(scrapy.Spider):
   name = "chloe"
   allowed_domains = ["mcmbuzz.com"]
   start_urls = [
       "http://www.mcmbuzz.com/2011/05/20/exclusive-free-manga-chloe-part-6/"
   ]

   def messages(self, msg_text):
     print "--------------------------------------------------------------------------------------"
     print msg_text
     print "--------------------------------------------------------------------------------------"
   
   def message_wo_format(self, msg_text):
     print msg_text

   def delete_file(self, file_name):
     os.remove(file_name)

   def make_zip(self, zip_filename, files_compress):
     self.messages("Building Zip File..."+ str(zip_filename))
     zip = zipfile.ZipFile(zip_filename, mode='w')
     for fname in files_compress:
       zip.write(fname)
       #Delete image file
       self.delete_file(fname)
     zip.close()
     return

   def get_images(self, img_link, img_filename):
     try:
       user_agent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
       values = {'name' : 'kirkjoserey',
                 'location' : 'United Federation',
                 'language' : 'English' }
       headers = { 'User-Agent' : user_agent }

       data = urllib.urlencode(values)
       #self.message_wo_format('Request: '+ img_link[:-4])
       req_img = urllib2.Request(img_link, data, headers)
                
       #self.message_wo_format('Open: '+ img_link)
       resp_img = urllib2.urlopen(req_img)

       imgData = resp_img.read()
       self.message_wo_format('Downloading: '+ img_filename)
       output = open(img_filename,'wb')
       output.write(imgData)
       output.close()
     except urllib2.HTTPError, e:
       self.messages('HTTP Access Error : ' + str(e.code))
     except urllib2.URLError, e:
       self.messages('URL Error : ' + str(e.reason))
     except Exception:
       self.messages("Error : " + traceback.format_exc())        
       pass 
     return

   def parse(self, response):  
     title = response.xpath('//title/text()').extract()[0].split('Manga:')[1]

     self.messages("Extracting pages from : " + title)

     img_name_prefix = response.xpath('//title/text()').extract()[0].split('Manga:')[1].split('-')[0]

     img_links = response.xpath("//a[@rel='lightbox[917]']/@href").extract()

     cbr_filename = title+'.cbr'
     count = 0
     files_images = []

     for img_link in img_links:
       count +=1
       #Auxiliary vars to get the filename, correctly
       bars = img_link.count("/")
       #--------------------------------------------
       img_filename = img_name_prefix+'-'+str(count).zfill(3)+'-'+img_link.split("/")[bars]
       
       #Only for test ...
       #self.message_wo_format("Page link : " + img_link)
       #self.message_wo_format("Page link : " + img_filename)
       self.get_images(img_link, img_filename)
       files_images.append(img_filename)
       
     if count>1:
       try:
         self.delete_file(cbr_filename)
       except:
         pass 
       #self.make_tar(cbr_filename, files_comp)
       self.make_zip(cbr_filename, files_images)
     else:
       self.messages("NO Pages in " + title + " :-( ")
     return
